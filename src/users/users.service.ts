import { Injectable, NotAcceptableException, NotFoundException } from '@nestjs/common';
import * as bcrypt from 'bcrypt'
import { scrypt as _scrypt, randomBytes } from 'crypto';
import { InjectRepository } from '@nestjs/typeorm';
import { Users } from './users.entity';
import { Repository } from 'typeorm';
import { UserCreateInterface, UserRetreiveAllInterface, UserRetrieveByEmailInterface, UserUpdateInterface } from './interfaces';

@Injectable()
export class UsersService {
    constructor(@InjectRepository(Users) private repo: Repository<Users>) { }


    async userRetreiveAll(payload: UserRetreiveAllInterface) {
        const skip: number = (payload.page - 1) * payload.limit
        const where: any = {}

        if (payload.searchName) {
            where.name = payload.searchName
        }

        if (payload.searchEmail) {
            where.email = payload.searchEmail
        }



        const [users, total] = await this.repo.findAndCount({ where, skip, take: payload.limit })

        return { users, total }
    }

    userRetreiveById(id: string) {
        return this.repo.findOneBy({ id })
    }

    async userRetrieveByEmail(payload: UserRetrieveByEmailInterface) {
        const user = await this.repo.findOneBy({ email: payload.email })

        if (!user) {
            throw new NotFoundException("User not found!")
        }

        return user
    }

    async create(payload: UserCreateInterface) {
        const user = await this.repo.findOneBy({ email: payload.email })

        if (user) {
            console.log(user);
            throw new NotAcceptableException("User already exists!")
        }

        const saltOrRounds = 10
        const hash = await bcrypt.hash(payload.password, saltOrRounds)

        const newUser = this.repo.create({ name: payload.name, email: payload.email, password: hash })

        return this.repo.save(newUser)
    }

    async update(payload: UserUpdateInterface) {
        const user = await this.repo.findOneBy({ id: payload.id })

        if (!user) {
            throw new NotFoundException("User not found!")
        }



        if (payload.password) {
            const saltOrRounds = 10
            const hash = await bcrypt.hash(payload.password, saltOrRounds)

            payload.password = hash
        }



        Object.assign(user, payload)

        return this.repo.save(user)
    }
}
