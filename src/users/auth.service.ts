import { Injectable, UnauthorizedException } from '@nestjs/common';
import * as bcrypt from 'bcrypt'
import { UsersService } from './users.service';
import { JwtService } from '@nestjs/jwt';
import { SignInInterface, JwtTokenInterface, JwtSignPayload } from "./interfaces"
import { UserDto } from './dtos/users.dto';

@Injectable()
export class AuthService {
    constructor(private usersService: UsersService, private jwtService: JwtService) { }

    async signIn(data: SignInInterface): Promise<JwtTokenInterface> {
        const user: UserDto = await this.usersService.userRetrieveByEmail({ email: data.email })
        const isMatch: boolean = await bcrypt.compare(data.password, user.password)

        if (!isMatch) {
            throw new UnauthorizedException()
        }

        const payload: JwtSignPayload = { user_id: user.id, email: user.email }

        return {
            access_token: await this.jwtService.signAsync(payload)
        }
    }
}
