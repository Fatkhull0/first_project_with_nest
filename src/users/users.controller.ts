import { Body, Controller, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dtos/create-user.dto';
import { UpdateUserDto } from './dtos/update-user.dto';
import { Serialize } from './interceptors/serialize.intercept';
import { UserCollecterDto, UserDto } from './dtos/users.dto';
import { AuthGuard } from 'src/guards/auth.guard';

@Controller('users')
@UseGuards(AuthGuard)
export class UsersController {
    constructor(private usersService: UsersService) { }

    @Get()
    @Serialize(UserCollecterDto)
    getAllUsers(@Query('page') page: number = 1, @Query('limit') limit: number = 10, @Query('searchName') searchName: string, @Query('searchEmail') searchEmail: string) {
        const users = this.usersService.userRetreiveAll({ page, limit, searchName, searchEmail })
        return users
    }

    @Get('/:id')
    @Serialize(UserDto)
    getSingleUser(@Param('id') id: string) {
        return this.usersService.userRetreiveById(id)
    }

    @Post()
    @Serialize(UserDto)
    createUser(@Body() body: CreateUserDto) {
        return this.usersService.create({ ...body })
    }

    @Patch('/:id')
    @Serialize(UserDto)
    patchUser(@Param('id') id: string, @Body() attrs: UpdateUserDto) {
        return this.usersService.update({ id, ...attrs })
    }
}

