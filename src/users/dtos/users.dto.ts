import { Exclude, Expose, Type } from "class-transformer";
import { IsEmail, IsNumber, IsString, IsUUID, ValidateNested } from "class-validator";

export class UserDto {
    @Expose()
    @IsUUID()
    id: string;

    @Expose()
    @IsString()
    name: string;

    @Expose()
    @IsEmail()
    email: string;

    @Exclude()
    @IsString()
    password: string
}

export class UserCollecterDto {
    @Expose()
    @Type(() => UserDto)
    @ValidateNested()
    users: UserDto[];

    @Expose()
    @IsNumber()
    total: number
}