export declare interface UserCreateInterface {
    name: string;
    email: string;
    password: string
}