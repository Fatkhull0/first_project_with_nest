export declare interface UserRetreiveAllInterface {
    page?: number;
    limit?: number;
    searchName?: string;
    searchEmail?: string;
}

export declare interface UserRetreiveByIdInterface {
    id: string
}

export declare interface UserRetrieveByEmailInterface {
    email: string
}