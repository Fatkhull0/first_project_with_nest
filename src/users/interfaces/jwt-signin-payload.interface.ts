export declare interface JwtSignPayload {
    user_id: string,
    email: string
}