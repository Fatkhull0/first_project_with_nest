export declare interface UserUpdateInterface {
    id: string;
    name?: string;
    email?: string;
    password?: string;
}