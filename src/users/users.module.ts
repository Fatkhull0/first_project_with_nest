import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Users } from './users.entity';
import { JwtModule } from '@nestjs/jwt';

const SECRET = process.env.JWT_SECRET


@Module({
    imports: [TypeOrmModule.forFeature([Users]), JwtModule.register({ global: true, secret: SECRET, signOptions: { expiresIn: '24h' } })],
    controllers: [
        AuthController,
        UsersController
    ],
    providers: [UsersService, AuthService]
})
export class UsersModule { }
